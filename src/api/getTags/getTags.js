import axios from "axios";

const getTags = async (id) => {
  const response = await axios.get(`/gitlab/projects/${id}/repository/tags`);
  return response.data;
};

export default getTags;
