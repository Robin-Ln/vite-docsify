import axios from "axios";

const getFileContent = async ({ id, filePath, ref }) => {
  const encodeFilePath = encodeURIComponent(filePath);
  const response = await axios.get(
    `/gitlab/projects/${id}/repository/files/${encodeFilePath}/raw`,
    {
      params: { ref: ref },
    }
  );
  return response.data;
};

export default getFileContent;
