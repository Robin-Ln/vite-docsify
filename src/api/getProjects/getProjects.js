import axios from "axios";

const getProjects = async (userId, page) => {
  const response = await axios.get(`/gitlab/users/${userId}/projects`, {
    params: { per_page: 100, page: page },
  });
  return response.data;
};

export default getProjects;
