import axios from "axios";

const getBranches = async (id) => {
  const response = await axios.get(`/gitlab/projects/${id}/repository/branches`);
  return response.data;
};

export default getBranches;
