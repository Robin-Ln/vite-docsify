import { getFileContent } from "../../api";
import { repoSelectedEventType, refSelectedEventType } from "../../constant";
import { getRepo, setRepo, removeRepo } from "../../util";

/*
 * -----------------
 * - Méthodes utils -
 * -----------------
 */

const createFileUrl = (id, filePath, ref) => {
  if (filePath.charAt(0) === "/") filePath = filePath.substring(1);
  filePath = encodeURIComponent(filePath);
  ref = encodeURIComponent(ref);
  return `/gitlab/projects/${id}/repository/files/${filePath}/raw?ref=${ref}`;
};

const replaceConfigArray = [
  {
    regExp: /!\[(?<description>.*)\]\((?<ressourcePath>.*)\)/gm,
    replaceValue: (match, content, repo) => {
      const { id, ref } = repo;
      const oldValue = match[0];
      const description = match.groups.description || "";
      const localRessourcePath = match.groups.ressourcePath || "";

      const remoteRessourcePath = createFileUrl(id, localRessourcePath, ref);

      return content.replace(
        oldValue,
        `![${description}](${remoteRessourcePath})`
      );
    },
  },
];

const replaceLocalPathToRemotePath = (content, repo) => {
  replaceConfigArray.forEach(({ regExp, replaceValue }) => {
    [...content.matchAll(regExp)].forEach((match) => {
      content = replaceValue(match, content, repo);
    });
  });

  return content;
};

/*
 * ---------------------------
 * - Configuration du plugin -
 * ---------------------------
 */

const gitlabRemotePlugin = (hook, vm) => {
  hook.init(() => {
    const { id, name, webUrl } = getRepo();
    if (id) {
      vm.config.name = name;
      vm.config.repo = webUrl;
    }

    window.addEventListener(repoSelectedEventType, ({ detail: repo }) => {
      setRepo(repo);
    });

    window.addEventListener(refSelectedEventType, ({ detail: repo }) => {
      const { ref } = repo;
      setRepo({ id, ref });
    });
  });

  hook.beforeEach(async (content, next) => {
    const repo = getRepo();
    const { id } = repo;

    if (id) {
      try {
        const contentWithLocalPath = await getFileContent(repo);
        const contentWithRemotePath = replaceLocalPathToRemotePath(
          contentWithLocalPath,
          repo
        );
        content = contentWithRemotePath;
      } catch (e) {
        alert(`Le projet ${repo.name} n'a pas de fichier ${repo.filePath}`);
        removeRepo(id);
      }
    }
    next(content);
  });
};

export default gitlabRemotePlugin;
