import "@tarekraafat/autocomplete.js/dist/css/autoComplete.02.css";
import autoComplete from "@tarekraafat/autocomplete.js";
import "./searchGitlabRepo.css";
import { getBranches, getProjects, getTags } from "../../api";
import {
  repoSelectedEventType,
  refSelectedEventType,
  repoStorageKey,
} from "../../constant";
import { getRepo } from "../../util";

/*
 * ------------------
 * - Méthodes utils -
 * ------------------
 */

const getAllProjects = async (user, page) => {
  const data = await getProjects(user, page);
  const repos = data.map(({ id, name, default_branch, web_url }) => ({
    id,
    name,
    filePath: "README.md",
    ref: default_branch,
    webUrl: web_url,
  }));

  return repos.length > 0
    ? [...repos, ...(await getAllProjects(user, ++page))]
    : repos;
};

const getProjectsFromStorage = async (user) => {
  const data = sessionStorage.getItem(repoStorageKey);
  const repreposFromStorageos = JSON.parse(data);
  if (repreposFromStorageos) {
    return repreposFromStorageos;
  }

  const repos = await getAllProjects(user, 0);
  sessionStorage.setItem(repoStorageKey, JSON.stringify(repos));
  return repos;
};

const getRefs = async (id) => {
  const branches = await getBranches(id);
  const tags = await getTags(id);

  return [...branches, ...tags].map(({ name }) => ({
    ref: name,
  }));
};

/*
 * -----------------------------------------------
 * - Méthode pour créer les barres de rechercher -
 * -----------------------------------------------
 */

const createSearchRepo = (user, name) => {
  const searchRepo = new autoComplete({
    selector: "#autoCompleteRepo",
    data: {
      src: async () => {
        // Loading placeholder text
        document
          .getElementById("autoCompleteRepo")
          .setAttribute("placeholder", "Loading...");
        // Fetch External Data Source
        const data = await getProjectsFromStorage(user);
        // Post Loading placeholder text
        document
          .getElementById("autoCompleteRepo")
          .setAttribute("placeholder", searchRepo.placeHolder);
        // Returns Fetched data
        return data;
      },
      keys: ["name", "id"],
      cache: true,
    },
    placeHolder: "Projets",
    resultsList: {
      element: (list, data) => {
        const info = document.createElement("p");
        if (data.results.length > 0) {
          info.innerHTML = ` <strong>${data.results.length}</strong> projets affichés sur <strong>${data.matches.length}</strong> résultats`;
        } else {
          info.innerHTML = `<strong>${data.matches.length}</strong> résultat pour la recherche <strong>"${data.query}"</strong>`;
        }
        list.prepend(info);
      },
      noResults: true,
      maxResults: 15,
      tabSelect: true,
    },
    resultItem: {
      element: (item, data) => {
        // Modify Results Item Style
        item.style = "display: flex; justify-content: space-between;";
        // Modify Results Item Content
        item.innerHTML = `
            <span class="resultItem_name">${data.value.name}</span>
            <span class="resultItem_id">${data.value.id}</span>
        `;
      },
      highlight: true,
    },
    events: {
      input: {
        selection: (event) => {
          const repoSelectedEvent = new CustomEvent(repoSelectedEventType, {
            detail: event.detail.selection.value,
          });
          window.dispatchEvent(repoSelectedEvent);
        },
      },
    },
  });
  if (name) {
    searchRepo.input.value = name;
  }
};

const createSearchRef = (id, ref) => {
  const searchRef = new autoComplete({
    selector: "#autoCompleteRef",
    data: {
      src: async () => {
        // Loading placeholder text
        document
          .getElementById("autoCompleteRef")
          .setAttribute("placeholder", "Loading...");

        const data = await getRefs(id);

        // Post Loading placeholder text
        document
          .getElementById("autoCompleteRef")
          .setAttribute("placeholder", searchRef.placeHolder);
        // Returns Fetched data
        return data;
      },
      keys: ["ref"],
      cache: true,
    },
    placeHolder: "Branches et tags",
    resultsList: {
      element: (list, data) => {
        const info = document.createElement("p");
        if (data.results.length > 0) {
          info.innerHTML = ` <strong>${data.results.length}</strong> branche(s) ou tag(s) affichés sur <strong>${data.matches.length}</strong> résultats`;
        } else {
          info.innerHTML = `<strong>${data.matches.length}</strong> résultat pour la recherche <strong>"${data.query}"</strong>`;
        }
        list.prepend(info);
      },
      noResults: true,
      maxResults: 15,
      tabSelect: true,
    },
    resultItem: {
      element: (item, data) => {
        // Modify Results Item Style
        item.style = "display: flex; justify-content: space-between;";
        // Modify Results Item Content
        item.innerHTML = `
            <span class="resultItem_name">${data.value.ref}</span>
        `;
      },
      highlight: true,
    },
    events: {
      input: {
        selection: (event) => {
          const repoSelectedEvent = new CustomEvent(refSelectedEventType, {
            detail: event.detail.selection.value,
          });
          window.dispatchEvent(repoSelectedEvent);
        },
      },
    },
  });

  if (ref) {
    searchRef.input.value = ref;
  }
};

const createTemplateHtml = (displayRef) => `
    <div class="autoComplete_content">
        <div class="autoComplete_wrapper">
          <input id="autoCompleteRepo" type="text">
          ${displayRef ? `<input id="autoCompleteRef" type="text">` : ``}
        </div>
    </div>
`;

/*
 * ---------------------------
 * - Configuration du plugin -
 * ---------------------------
 */

const searchGitlabRepo = (hook, vm) => {
  hook.afterEach((html, next) => {
    const { id } = getRepo();
    next(createTemplateHtml(id) + html);
  });

  hook.doneEach(() => {
    const { id, name, ref } = getRepo();
    createSearchRepo(vm.config.gitlabUser, name);
    if (id) {
      createSearchRef(id, ref);
    }
  });
};

export default searchGitlabRepo;
