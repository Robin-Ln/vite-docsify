import "docsify/lib/themes/vue.css";
import mermaid from "mermaid";
import { searchGitlabRepo, remoteGitlab } from "./plugin";

window.$docsify = {
  name: "vite-docsify",
  search: "auto",
  repo: "https://gitlab.com/Robin-Ln/vite-docsify",
  gitlabUser: "Robin-Ln",
  markdown: {
    renderer: {
      code: function (code, lang) {
        if (lang === "mermaid") {
          return `<div class="mermaid">${mermaid.render(
            "mermaid-svg",
            code
          )}</div>`;
        }
        return this.origin.code.apply(this, arguments);
      },
    },
  },
  plugins: [searchGitlabRepo, remoteGitlab],
};

// Il faut importé les script docsify après l'initialisation de la variable docsify
import("docsify/lib/docsify").then(() => {});
import("docsify/lib/plugins/search").then(() => {});
