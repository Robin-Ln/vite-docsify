const removeRepo = (id) => {
  sessionStorage.removeItem(id);
  const url = new URL(window.location.href);
  url.searchParams.delete("id");
  url.searchParams.delete("ref");
  window.location.replace(url);
};

export default removeRepo;
