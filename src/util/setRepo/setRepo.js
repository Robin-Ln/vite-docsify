const setRepo = (repo) => {
  const { id, ref } = repo;

  // Configuration des paramètres de l'url
  const url = new URL(window.location.href);
  url.searchParams.set("id", id);
  url.searchParams.set("ref", ref);

  // Configuration du cache
  const oldRepo = JSON.parse(sessionStorage.getItem(id));
  sessionStorage.setItem(id, JSON.stringify({ ...oldRepo, ...repo }));

  // Redirection
  window.location.replace(url);
};

export default setRepo;
