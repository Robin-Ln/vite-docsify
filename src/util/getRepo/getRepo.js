const getRepo = () => {
  // Récupération des paramètres de l'url
  const url = new URL(window.location.href);
  const id = url.searchParams.get("id");

  // Récupération du répo depuis le storage
  const remoteRepoJson = sessionStorage.getItem(id);
  return { ...JSON.parse(remoteRepoJson) };
};

export default getRepo;
