const repoSelectedEventType = "REPO_SELECTED_EVENT_TYPE";
const refSelectedEventType = "REF_SELECTED_EVENT_TYPE";
const repoStorageKey = "REPOS";

export { repoSelectedEventType, refSelectedEventType, repoStorageKey };
