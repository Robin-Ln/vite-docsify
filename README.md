# Projet de documentation avec vitejs

> L'objectif de ce projet est de faciliter les potentielles évolution en utilidant vitejs

## Les plugins

### La recherche de répositories gitlab

| Outils                                                                                  | description                                                |
| --------------------------------------------------------------------------------------- | ---------------------------------------------------------- |
| [vitejs](https://vitejs.dev/) - [server.proxy](https://vitejs.dev/config/#server-proxy) | Utilisation d'un proxy http pour faire les appels api      |
| [autoComplete](https://tarekraafat.github.io/autoComplete.js/#/)                        | Composant ui d'une liste déroulate pour faire la recherche |

> TODO :
> - mieux géré les event
> - initialiaser la partie i18n
> - créer un service pour géré les params
> - récupérer tous les projets et pas juste 20
> - IDEM pour les version et les branchs
