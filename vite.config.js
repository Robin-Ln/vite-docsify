import { defineConfig } from "vite";

export default defineConfig({
  server: {
    proxy: {
      "/gitlab": {
        target: "https://gitlab.com/api/v4",
        changeOrigin: true,
        headers: {
          "PRIVATE-TOKEN": "glpat-frxobyyb2hB4raYm3aQd",
        },
        rewrite: (path) => path.replace(/^\/gitlab/, ""),
      },
    },
  },
  build: {
    outDir: "public",
    rollupOptions: {
      output: {
        manualChunks: {
          mermaid: ["mermaid"],
        },
      },
    },
  },
});
